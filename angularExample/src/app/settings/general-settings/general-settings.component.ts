import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.css']
})
export class GeneralSettingsComponent implements OnInit {
  public bar:boolean = false;
  public media:boolean = false;
  public db:boolean = false;
  constructor() { }

  ngOnInit() {
	  this.bar = true;
  }
  Bar(){
	   this.bar = true;
	   this.media = false;
	   this.db = false;
  }
  Media(){
	  this.bar = false;
	  this.media = true;
	  this.db = false;
  }
  Db(){
	  this.bar = false;
	  this.media = false;
	  this.db = true;
  }
	  
  

}
