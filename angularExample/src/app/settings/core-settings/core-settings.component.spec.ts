import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreSettingsComponent } from './core-settings.component';

describe('CoreSettingsComponent', () => {
  let component: CoreSettingsComponent;
  let fixture: ComponentFixture<CoreSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
