import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './settings.component';

import { UserSettingComponent } from './user-setting/user-setting.component';
import { CoreSettingsComponent } from './core-settings/core-settings.component';
import { GeneralSettingsComponent } from './general-settings/general-settings.component';

const routes: Routes = [
	{ 	path: '', component: SettingsComponent,
		children:[
			{ path: 'user-setting', component: UserSettingComponent },
			{ path: 'core-setting', component: CoreSettingsComponent },
			{ path: 'general-setting', component: GeneralSettingsComponent }
		]
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
