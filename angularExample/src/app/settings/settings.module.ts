import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import {MatButtonModule} from '@angular/material/button';
import { UserSettingComponent } from './user-setting/user-setting.component';
import { CoreSettingsComponent } from './core-settings/core-settings.component';
import { GeneralSettingsComponent } from './general-settings/general-settings.component';
import { BarComponent } from './bar/bar.component';
import { MediaComponent } from './media/media.component';
import { DbComponent } from './db/db.component';
import { InputFocusDirective } from '../input-focus.directive';
@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [InputFocusDirective,SettingsComponent, UserSettingComponent, CoreSettingsComponent, GeneralSettingsComponent, BarComponent, MediaComponent, DbComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
	MatButtonModule
  ]
})
export class SettingsModule { }
