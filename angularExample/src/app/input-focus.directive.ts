import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appInputFocus]'
})
export class InputFocusDirective {
  public _el;
  public input;
  constructor(el: ElementRef) {
    this._el = el;
    // classList
   }
  @HostListener('keyup', ['$event']) onKeyDown(e) {
    console.log(this.getAllinputType());
    for(let i=0;i<this.getAllinputType().length;i++){
      if((this.getAllinputType()[i].type === 'text' || this.getAllinputType()[i].type === 'password' || this.getAllinputType()[i].type === "email")){
        if(e.target.id  != this.getAllinputType()[i].id && e.target.value.length == e.target.maxLength && this.getAllinputType()[i].disabled==false){ 
          if(this.getMyIndex(this.getAllinputType()[i].id) > this.getMyIndex(e.target.id)){
            this.getAllinputType()[i].focus();
            break;
          }
          if(this.totalInput() === this.getMyIndex(e.target.id)){
            this.getAllinputType()[0].focus();
            break;
          }
        }
        
      }
    }
  }
  getAllinputType(){
    return  document.getElementsByTagName('input');
  }
  getMyIndex(id){
    for(let i=0;i<this.getAllinputType().length;i++){
      if(this.getAllinputType()[i].id ===id){
        return i;
      }
    }
  }
  totalInput(){
    let count = -1;
    for(let i=0;i<this.getAllinputType().length;i++){
      if((this.getAllinputType()[i].type === 'text' || this.getAllinputType()[i].type === 'password' || this.getAllinputType()[i].type === "email")){
        count = count+1;
      }
    }
    return count;
  }
}
